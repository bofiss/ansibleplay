#!/usr/bin/env bash
#install Ansible
apt-get -y install software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt-get update
apt-get -y install ansible

# copy playbooks into /home/vagrantup
cp -a /vagrant/playbooks/* /home/vagrant
chown -R vagrant:vagrant /home/vagrant

# configure internal hosts file
cat >> /etc/hosts <<EOL

10.0.15.10 manager
10.0.15.28 web8
10.0.15.29 web9

EOL
